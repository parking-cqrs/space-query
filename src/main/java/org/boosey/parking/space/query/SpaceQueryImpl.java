package org.boosey.parking.space.query;

import java.util.List;
import java.util.Optional;
import org.boosey.parking.common.events.SpaceAllDeletedEvent;
import org.boosey.parking.common.events.SpaceCreatedEvent;
import org.boosey.parking.common.events.SpaceDeletedEvent;
import org.boosey.parking.common.events.CreateSpaceCommandEvent.CreateSpaceCommandEventData;
import org.boosey.parking.common.events.DeleteAllSpacesCommandEvent.DeleteAllSpacesCommandEventData;
import org.boosey.parking.common.events.DeleteSpaceCommandEvent.DeleteSpaceCommandEventData;
import org.boosey.parking.common.exceptions.ItemDoesNotExistException;
import org.boosey.parking.common.servicebase.QueryServiceBase;
import org.boosey.parking.common.servicebase.QueryServiceInterface;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.UUID;
import org.boosey.parking.common.entities.Space;

@Slf4j
@ApplicationScoped
public class SpaceQueryImpl extends QueryServiceBase<Space> implements QueryServiceInterface<Space> {

    @Inject SpaceCreatedEvent spaceCreatedComplete;
    @Inject SpaceDeletedEvent spaceDeletedComplete;
    @Inject SpaceAllDeletedEvent spaceAllDeletedComplete;

    public SpaceQueryImpl() { }

    @PostConstruct
    public void init() {
        // this.configure(Space.class);
    }

    public static SpaceQueryImpl newSpaceQueryImpl() {
        SpaceQueryImpl sqi = new SpaceQueryImpl();
        sqi.configure(Space.class);
        return sqi;
    }

    @Transactional
    public List<Space> listAll() {
        List<Space> returnList = null;
        try {
            log.info("About to call listAll");
            returnList = Space.listAll();
            log.info("returnList: {}", returnList);            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnList;
    }

    public Optional<Space> findByNumberOptional(String number) {
        return Space.findByNumberOptional(number);
    }

    public Boolean spaceNumberExists(String number) {
        return this.findByNumberOptional(number).isPresent();
    }

    public Boolean spaceNumberDoesNotExist(String number) {
        return !(this.spaceNumberExists(number));
    }

    public Boolean exists(Space s) {
        return Space.exists(s);
    }

    @Incoming("create-space-command-incoming")
    public void handleSpaceCreatedCommand(String evt) {
        CreateSpaceCommandEventData evtData = this.getEventData(evt, CreateSpaceCommandEventData.class);
        Space spc = new Space();
        spc.id = evtData.spaceId;
        spc.number = evtData.number;

        executeTransaction(() -> {
            spc.persistAndFlush();
            return true;
        });    
        spaceCreatedComplete.with(UUID.fromString(evtData.commandId), spc.id, spc.number).emit();
    }

    @Incoming("delete-all-spaces-command-incoming")
    public void handleAllSpacesDeletedCommand(String evt) {
        DeleteAllSpacesCommandEventData evtData = this.getEventData(evt, DeleteAllSpacesCommandEventData.class);
        executeTransaction(() -> {
            Space.deleteAll();
            return true;
        });    
        spaceAllDeletedComplete.with(UUID.fromString(evtData.commandId), evtData.count).emit();
    }

    @Incoming("delete-space-command-incoming")
    public void handleSpaceDeletedCommand(String evt) {
        DeleteSpaceCommandEventData evtData = this.getEventData(evt, DeleteSpaceCommandEventData.class);
        if(evtData.isIndividualDeletion) {
            executeTransaction(() -> {
                Space spc = null;
				try {
					spc = this.getById(evtData.spaceId);
				} catch (ItemDoesNotExistException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
				}
                spc.delete();
                return true;
            });    
        }
        spaceDeletedComplete.with(UUID.fromString(evtData.commandId), UUID.fromString(evtData.spaceId), evtData.isIndividualDeletion).emit();
    }
}