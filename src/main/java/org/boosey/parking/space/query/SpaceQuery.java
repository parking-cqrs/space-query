package org.boosey.parking.space.query;

import java.util.List;
import javax.enterprise.context.control.ActivateRequestContext;
import javax.inject.Singleton;
import io.grpc.stub.StreamObserver;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import lombok.extern.slf4j.Slf4j;
import org.boosey.parking.common.entities.Space;

@Slf4j
@Singleton
public class SpaceQuery extends SpaceQueryGrpc.SpaceQueryImplBase {

    SpaceQueryImpl spaceQueryImpl = SpaceQueryImpl.newSpaceQueryImpl();

    @Override
    @ActivateRequestContext
    public void listSpaces(SpaceListRequest request, StreamObserver<SpaceListReply> responsObserver) {

        log.info("spaceQueryImpl: {} ", spaceQueryImpl);
        Uni.createFrom().item(() -> {
                // @SuppressWarnings("unchecked")
                List<Space> sl = (List<Space>) spaceQueryImpl.listAll();
                return sl;
            })
            .subscribeOn(Infrastructure.getDefaultWorkerPool())
            .subscribe().with(
                sl -> {
                    log.error("Got list: {}", sl);
                    SpaceListReply.Builder slr = SpaceListReply.newBuilder();
                    sl.forEach(s -> {
                        log.error("Adding Space: {}", s);
                        SpaceBuf sb = SpaceBuf.newBuilder()
                            // .setId(s.id)
                            .setNumber(s.number)
                            // .setOwnerId(s.ownerId)
                            // .setResidentId(s.residentId)
                            // .setResidentApprovedByOwner(s.residentApprovedByOwner)
                            .build();

                            log.error("Adding SpaceBuf: {}", sb);
                            slr.addSpace(sb);
                    });

                    log.error("onNext");
                    responsObserver.onNext(slr.build());
                    responsObserver.onCompleted();
                    log.error("Completed");
                },

                failure -> {
                    log.error("Error getting list: ", failure);
                    throw new RuntimeException("Error getting list");
                });
    }

    @Override
    public void spaceNumberExists(SpaceNumberExistsRequest request, StreamObserver<SpaceNumberExistsReply> responseObserver) {
        responseObserver.onNext(SpaceNumberExistsReply.newBuilder().setDoesExist(true).build());
        responseObserver.onCompleted();
    }

    @Override
    public void spaceNumberDoesNotExists(SpaceNumberDoesNotExistsRequest request, StreamObserver<SpaceNumberDoesNotExistsReply> responseObserver) {
        responseObserver.onNext(SpaceNumberDoesNotExistsReply.newBuilder().setDoesExist(true).build());
        responseObserver.onCompleted();
    }

}